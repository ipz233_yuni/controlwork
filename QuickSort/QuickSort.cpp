#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <chrono>

#define GETTIME std::chrono::steady_clock::now
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>

// ������� ��� ����� ������� �� ����� ����������
void swap(int* a, int* b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

// ������� ��� �������� ������ �� ���������� ������� �������� ��������
int partition(int arr[], int low, int high) {
    int pivot = arr[high];  // �������� �������� ������� �� �������
    int i = low - 1;  // ������ ������� ��������

    for (int j = low; j < high; j++) {
        // ���� �������� ������� ������ �� �������
        if (arr[j] < pivot) {
            i++;  // �������� ������ ������� ��������
            swap(&arr[i], &arr[j]);  // �������� ��������
        }
    }
    swap(&arr[i + 1], &arr[high]);  // �������� ������� ������� �� ���� ����������� ����
    return (i + 1);  // ��������� ������ �������� ��������
}

// ������� ������� Quicksort
void quicksort(int arr[], int low, int high) {
    if (low < high) {
        // �������� ������ �������� ��������
        int pivotIndex = partition(arr, low, high);

        // ���������� ������� ��� �� ����� ������� ������
        quicksort(arr, low, pivotIndex - 1);
        quicksort(arr, pivotIndex + 1, high);
    }
}

// ������� ��� ��������� ������ �� �����
void printArray(int arr[], int size) {
    for (int i = 0; i < size; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int main() {
    int arr[100000];
    int size;

    printf("Enter size of array: ");
    scanf_s("%d", &size);

    // ��������� ��������� ����� � �����
    srand(time(NULL));
    for (int i = 0; i < size; i++) {
        arr[i] = rand() % 100;  // �������� �������� ����� �� 0 �� 99
    }

    printf("\nUnsorted array:\n");
    printArray(arr, size);

    auto begin = GETTIME();
    quicksort(arr, 0, size - 1);
    auto end = GETTIME();

    printf("\nSorted array:\n");
    printArray(arr, size);

    auto elapsed_ns = CALCTIME(end - begin);
    printf("\nSort time: %lld ns\n", elapsed_ns.count());

    return 0;
}